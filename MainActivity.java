package trabajo.primer.formulario;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import trabajo.primer.formulario.fragmentos.FragmentContacto;
import trabajo.primer.formulario.fragmentos.FragmentPersonal;
import trabajo.primer.formulario.fragmentos.FragmentSesion;

public class MainActivity extends AppCompatActivity implements
        FragmentSesion.OnFragmentInteractionListener,
        FragmentContacto.OnFragmentInteractionListener,
        FragmentPersonal.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        FragmentPersonal FragmentPersonal = new FragmentPersonal();

        transaction.replace(R.id.f1Contenedor, FragmentPersonal).commit();

    }

    @Override
    public void onFragmentInteraction(String nombreFragmento, String evento) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        //Pasar a datos de contacto
        if(nombreFragmento.equals("FragmentPersonal")){
            if(evento.equals("BTN_ACEPTAR_CLICK")){
                FragmentContacto fragment = new FragmentContacto();
                transaction.replace(R.id.f1Contenedor,fragment).commit();
            }
        }

        // Pasar a datos de inicio de sesion
        if(nombreFragmento.equals("FragmentContacto")){
            if(evento.equals("BTN_ACEPTAR_CLICK")){
                FragmentSesion fragment = new FragmentSesion();
                transaction.replace(R.id.f1Contenedor,fragment).commit();
            }
        }

        // Mostrar contrasena
        if(nombreFragmento.equals("FragmentSesion")){
            if(evento.equals("BTN_ACEPTAR_CLICK")){


            }
        }

    }


}