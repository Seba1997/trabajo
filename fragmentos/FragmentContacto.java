package trabajo.primer.formulario.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import trabajo.primer.formulario.R;

public class FragmentContacto extends Fragment {

    private OnFragmentInteractionListener mListener;

    public FragmentContacto() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                   Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_fragment_contacto, container, false);

        final EditText etCorreo = (EditText) layout.findViewById(R.id.etCorreo);
        final EditText etTelefono = (EditText) layout.findViewById(R.id.etTelefono);
        final Button btnAceptar = (Button) layout.findViewById(R.id.btnAceptar);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String correo = etCorreo.getText().toString().trim();
                if(!correo.isEmpty()) {
                    mListener.onFragmentInteraction("FragmentContacto","BTN_ACEPTAR_CLICK");
                } else {
                    Toast.makeText(getContext(), "Debe ingresar un correo", Toast.LENGTH_SHORT).show();

                    }
            }
        });

        return layout;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("","");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String nombreFragmento, String evento);
    }
}
