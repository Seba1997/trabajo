package trabajo.primer.formulario.fragmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import trabajo.primer.formulario.R;

public class FragmentPersonal extends Fragment {

    private OnFragmentInteractionListener mListener;

    public FragmentPersonal() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_fragment_personal, container, false);

        final EditText etNombre = (EditText) layout.findViewById(R.id.etNombre);
        final EditText etEdad = (EditText) layout.findViewById(R.id.etEdad);
        final Button btnAceptar = (Button) layout.findViewById(R.id.btnAceptar);

        btnAceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = etNombre.getText().toString().trim();
                if(!nombre.isEmpty()) {
                    mListener.onFragmentInteraction("FragmentPersonal", "BTN_ACEPTAR_CLICK");
                } else {
                    Toast.makeText(getContext(), "Debe ingresar un nombre", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return layout;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener ");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("","");
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String nombreFragmento, String evento);
    }
}
